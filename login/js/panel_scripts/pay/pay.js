var $ = jQuery;
firebase.auth().onAuthStateChanged(function (user) {
  if (user) {
    // user is signed in
    //firebase auth current user
    var user = firebase.auth().currentUser
    //getting the user's email
    var email = user.email
    //getting the user's name
    var name = user.displayName

    console.log(name);

    if (name === "null") {
      updateName()
    }

    if (name != "null"){
      //inner html name
      $('#user-name-dropdown').html(name);
    }
    else {
      //inner html name
      $('#user-name-dropdown').html("user");
    }

    if(email != "null"){
      $('#user-email-dropdown').html(email);
    }
    else {
      $('#user-email-dropdown').html(name+"@indiakart.bitbucket.io");
    }

  } else {
    // user is signed out
    location.assign("index.html");
  }
});

//signout firebase function
function signout() {
  firebase.auth().signOut().then(function () {
    location.assign("index.html");
  });
}

//telling a fake price of shipping with the use of Sweet alert popups
function showPrice() {
  Swal.fire({
    title: "Price of shipping",
    text: "The price of shipping according to the location given is is ₹5,000",
    icon: "success",
    confirmButtonText: "Ok"
  });
}

function updateName(){
  name_user_val = localStorage.getItem('user_name')
  if (name_user_val === "null") {
    var user = firebase.auth().currentUser;
    //setting the name of the user firebase
    user.updateProfile({
        displayName: "user",
    })
  }
  else {
    var user = firebase.auth().currentUser;
    //setting the name of the user firebase
    user.updateProfile({
        displayName: name_user_val,
    })
  }
}